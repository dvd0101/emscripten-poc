#include <emscripten/bind.h>
#include <vector>
#include <boost/geometry.hpp>
#include <boost/geometry/geometries/register/point.hpp>
#include <sstream>

namespace bg = boost::geometry;

struct point {
	double x;
	double y;
	double z;
};

BOOST_GEOMETRY_REGISTER_POINT_3D(point, double, bg::cs::cartesian, x, y, z)

using segment = bg::model::segment<point>;

double dis(point a, point b) {
	return bg::distance(a, b);
}

std::vector<point> intersection(segment a, segment b) {
	std::vector<point> r;
	bg::intersection(a, b, r);
	return r;
}

template<typename Geometry>
std::string ws(const Geometry& g) {
	std::ostringstream out;
	out << bg::wkt(g);
	return out.str();
}

int sum(intptr_t ptr, int length) {
	auto buffer = reinterpret_cast<char*>(ptr);
	int r = 0;
	for (int ix=0; ix<length; ix++) {
		r += buffer[ix];
	}
	//buffer[0] = 100;
	return r;
}

using namespace emscripten;
EMSCRIPTEN_BINDINGS(my_module) {
	value_object<point>("point")
        .field("x", &point::x)
        .field("y", &point::y)
        .field("z", &point::z);

    class_<segment>("segment")
    	.constructor<point, point>()
    	.property("p0", +[](const segment& s) { return s.first; })
    	.property("p1", +[](const segment& s) { return s.first; })
    	.function("toString", +[](segment& s) { return ws(s); })
    	;

    register_vector<point>("VectorPoint");

    function("dis", &dis);
    function("intersection", &intersection);
    function("sum", &sum, allow_raw_pointers());
}
