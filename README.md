emscripten test
===============

proof of concept of how to expose a c++ class and some functions that use boost.geometry

To compile
----------

```
em++ -Wall -Wextra --bind -std=c++14 -I<path/to/boost> -O3 test.cpp -o test.js
```
